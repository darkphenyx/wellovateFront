import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'

import { store } from './store/store';
import {routes} from './routes';

Vue.use(VueRouter);

Vue.filter('capitalize', (str) => {
    return str.toLowerCase().replace(/(^| )(\w)/g, s => s.toUpperCase());
});

const router = new VueRouter({
    mode: 'history',
    routes
});

new Vue({
  el: '#app',
    store,
    router,
  render: h => h(App)
})
