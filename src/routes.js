/**
 * Created by GlaDOS on 6/10/2017.
 */
import Home from './components/Home.vue';
import Login from './components/login/Login.vue';
import AddUser from './components/user/AddUser.vue';
import AddQuestion from './components/question/AddQuestion.vue';
import SeeQuestion from './components/question/SeeQuestion.vue';
import CreateQuestion from './components/question/questionnaire/Questionnaire.vue';
import AssignUsers from './components/question/questionnaire/AssignUsers.vue';
import EmailUsers from './components/question/questionnaire/EmailUsers.vue';
import PageNotFound from './components/404/PageNotFound.vue';

export const routes = [
    { path: '/', component: Home },
    { path: '/login', component: Login },
    { path: '/new_user', component: AddUser},
    { path: '/add_question', component: AddQuestion },
    { path: '/edit_question', component: SeeQuestion },
    { path: '/create_questionnaire', component: CreateQuestion },
    { path: '/assign_user', component: AssignUsers },
    { path: '/email_questionnaire', component: EmailUsers },
    { path: '*', component: PageNotFound }
];