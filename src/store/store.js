/**
 * Created by GlaDOS on 6/10/2017.
 */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        userInfo:{},
        loggedIn: false,
        quest_types:{},
        quest_type_array:{},
        liveAPI: 'http://www.admin.wellovate.com/wellovateAPI/public/index.php/api',
        testAPI: 'http://wellovate/api'
    },
    getters: {
        getInfo: state => {
            return state.userInfo;
        },
        getQuestType: state => {
            return state.quest_types;
        },
        getQuestTypeArray: state => {
            return state.quest_type_array;
        }
    },
    mutations: {
        updateUser: (state, payload) => {
            state.userInfo = payload;
        },
        changeLogin: (state, payload) => {
            state.loggedIn = payload;
        },
        emptyUser: state => {
            state.userInfo = [];
        },
        updateQuestType: (state, payload) => {
            state.quest_types = payload;
        },
        updateQuestTypeArray: (state, payload) => {
            state.quest_type_array = payload;
        }
    },
    actions:{
        updateUser: (context, payload) => {
            context.commit('updateUser', payload);
        },
        changeLogin: (context, payload) => {
            context.commit('changeLogin', payload);
        },
        emptyUser: (context, payload) => {
            context.commit('emptyUser', payload)
        },
        updateQuestType: (context, payload) => {
            context.commit('updateQuestType', payload);
        },
        updateQuestTypeArray: (context, payload) => {
            context.commit('updateQuestTypeArray', payload);
        }
    }
});